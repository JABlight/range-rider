module.exports = function (grunt) {

    // Project Config
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                mangle: true
            },
            my_target: {
                files: {
                    'dist/bundle.min.js': ['src/rangerider.js']
                }
            }
        }
    });

    // Load uglify
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default Task
    grunt.registerTask('default', ['uglify']);
};