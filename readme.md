# Range Rider
## HTML5 vanilla js range slider

### To Run
Run ```grunt``` in the command line

Use bundle.min.js and rangerider.css in your project

### To Use
```javascript
// element to attach the slider to
var anchor = document.getElementById('slider-container');

// create our dual range slider
rangeRider({
    anchor: anchor,
    range: [0, 10],
    start: [0, 10]
});
```

### Demo
http://jsfiddle.net/narklord/gfgdpegd/
